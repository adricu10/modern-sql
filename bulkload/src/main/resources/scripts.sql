TRUNCATE TABLE fct_trip;

INSERT INTO fct_trip
  SELECT
    to_char(start_time, 'YYYYMMdd') :: INT                   AS start_day_id,
    extract(HOUR FROM start_time)                            AS start_hour_id,
    to_char(start_time, 'HH24:MI') :: TIME WITHOUT TIME ZONE AS start_time,
    start_terminal_id,
    to_char(end_time, 'YYYYMMdd') :: INT                     AS end_day_id,
    extract(HOUR FROM end_time)                              AS end_hour_id,
    to_char(end_time, 'HH24:MI') :: TIME WITHOUT TIME ZONE   AS end_time,
    end_terminal_id,
    bike_id,
    subscription_id,
    duration_minutes
  FROM trips_q4_2;

SELECT count(*)
FROM fct_trip;

SELECT count(*)
FROM trips_q1_2;

CREATE TABLE fct_trip AS
  SELECT
    to_char(start_time, 'YYYYMMdd') :: INT                   AS start_day_id,
    extract(HOUR FROM start_time)                            AS start_hour_id,
    to_char(start_time, 'HH24:MI') :: TIME WITHOUT TIME ZONE AS start_time,
    start_terminal_id,
    to_char(end_time, 'YYYYMMdd') :: INT                     AS end_day_id,
    extract(HOUR FROM end_time)                              AS end_hour_id,
    to_char(end_time, 'HH24:MI') :: TIME WITHOUT TIME ZONE   AS end_time,
    end_terminal_id,
    bike_id,
    subscription_id,
    duration_minutes
  FROM trips_2;

SELECT *
FROM trips;

TRUNCATE TABLE trips_q2_2;
INSERT INTO trips_q2_2
  SELECT
    to_timestamp(start_time, 'MM/dd/YYYY HH24:MI:SS') AS start_time,
    dts.id                                            AS start_terminal_id,
    to_timestamp(end_time, 'MM/dd/YYYY HH24:MI:SS')   AS end_time,
    dte.id                                            AS end_terminal_id,
    b.id                                              AS bike_id,
    s.id                                              AS subscription_id,
    (duration_ms :: BIGINT) / 1000 / 60               AS duration_minutes
  FROM trips_q2 t
    INNER JOIN dim_terminal dts ON dts.name = t.start_terminal
    INNER JOIN dim_terminal dte ON dte.name = t.end_terminal
    INNER JOIN dim_bike b ON b.plate = t.bike_number
    INNER JOIN dim_subscription s ON s.name = lower(t.subscription_type);

SELECT count(*)
FROM trips_q2_2;

TRUNCATE TABLE trips_q4_2;
INSERT INTO trips_q4_2
  SELECT
    to_timestamp(start_date, 'MM/dd/YYYY HH24:MI:SS') AS start_time,
    dts.id                                            AS start_terminal_id,
    to_timestamp(start_date, 'MM/dd/YYYY HH24:MI:SS') AS end_time,
    dte.id                                            AS end_terminal_id,
    b.id                                              AS bike_id,
    s.id                                              AS subscription_id,
    (duration :: BIGINT) / 1000 / 60                  AS duration_minutes
  FROM trips_q4_weird t
    INNER JOIN dim_terminal dts ON dts.name = t.start_station
    INNER JOIN dim_terminal dte ON dte.name = t.end_station
    INNER JOIN dim_bike b ON b.plate = t.bike_number
    INNER JOIN dim_subscription s ON s.name = lower(t.member_type);

SELECT count(*)
FROM trips_q4_2;


SELECT max(duration_ms :: BIGINT) / 1000 / 60
FROM trips_q1;

CREATE TABLE trips_q4
(
  duration_ms       VARCHAR(250),
  start_time        VARCHAR(250),
  start_terminal    VARCHAR(250),
  end_time          VARCHAR(250),
  end_terminal      VARCHAR(250),
  bike_number       VARCHAR(20),
  subscription_type VARCHAR(20)
);

DROP TABLE trips_q3_weird;
CREATE TABLE trips_q4_weird (
  duration             INT,
  start_date           VARCHAR(50),
  end_date             VARCHAR(50),
  start_station_number INT,
  start_station        VARCHAR(100),
  end_station_number   INT,
  end_station          VARCHAR(100),
  bike_number          VARCHAR(50),
  member_type          VARCHAR(50)
);

COPY trips_q4_weird FROM '/Users/guillermo/src/bunt/formacion/modernsql/bulkload/src/main/resources/2015-Q4-Trips-History-Data.csv' WITH (
FORMAT CSV, DELIMITER ',', HEADER TRUE );

SELECT
  is_weekend,
  b.type,
  count(*)
FROM fct_trip t
  INNER JOIN dim_day d ON d.day_id = t.start_day_id
  INNER JOIN dim_bike b ON b.id = t.bike_id
GROUP BY is_weekend, b.type;

SELECT count(*)
FROM fct_trip;


SELECT
  json_agg(name),
  count(*)
FROM dim_terminal
GROUP BY
  substr(geohash, 0, 7);


TRUNCATE TABLE dim_bike;


INSERT INTO dim_bike
  SELECT
    nextval('dim_bike_id_seq')             AS id,
    bike_number                            AS plate,
    CASE
    WHEN substr(bike_number, 0, 3) = 'W0'
      THEN 'road'
    ELSE 'city'
    END                                    AS type,
    floor(random() * (2015 - 2012) + 2012) AS purchase_year
  FROM bikes;


ALTER SEQUENCE dim_bike_id_seq RESTART WITH 1;
CREATE TABLE bikes AS SELECT
                        DISTINCT (plate)
                      FROM dim_bike;


SELECT
    q.start_station,
    b.id
FROM trips_q4_weird q
left join dim_terminal b on b.name = q.start_station
where b.id IS NULL;

INSERT INTO dim_bike
  SELECT
    nextval('dim_bike_id_seq')             AS id,
    plate,
    CASE
    WHEN substr(plate, 0, 3) = 'W0'
      THEN 'road'
    ELSE 'city'
    END                                    AS type,
    floor(random() * (2015 - 2012) + 2012) AS purchase_year
  FROM bikes;

SELECT count(*)
FROM dim_bike;