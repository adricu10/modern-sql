package com.buntplanet.formacion.modernsql;

import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

class Setup {
  private static final Logger logger = LoggerFactory.getLogger(Setup.class);

  static void prepareDBNoIndexes(Connection conn) throws SQLException {
    conn.prepareStatement("DROP TABLE IF EXISTS trips").execute();
    logger.info("Tabla 'trips' borrada si existe");
    conn.prepareStatement("" +
        "CREATE TABLE trips (" +
        "   duration_ms VARCHAR(250), " +
        "   start_time VARCHAR(250), " +
        "   start_terminal VARCHAR(250), " +
        "   end_time VARCHAR(250), " +
        "   end_terminal VARCHAR(250), " +
        "   bike_number VARCHAR(20), " +
        "   subscription_type VARCHAR(20)" +
        ")").execute();

    logger.info("Tabla 'trips' creada");
  }

  static void createIndexes(Connection conn) throws SQLException {
    conn.prepareStatement("CREATE INDEX trips_duration ON trips(duration_ms)").execute();
    conn.prepareStatement("CREATE INDEX trips_start_time ON trips(start_time)").execute();
    conn.prepareStatement("CREATE INDEX trips_end_time ON trips(end_time)").execute();
    logger.info("Índices en tabla 'trips' creados");
  }

  static Connection getSingleConnection() throws SQLException {
    return DriverManager.getConnection("jdbc:postgresql://localhost/postgres?user=guillermo&password=");
  }

  static HikariDataSource getDataSource() {
    HikariDataSource dataSource = new HikariDataSource();
    dataSource.setDriverClassName("org.postgresql.Driver");
    dataSource.setJdbcUrl("jdbc:postgresql://localhost/modernsql");
    dataSource.setUsername("guillermo");
    dataSource.setPassword("");
    dataSource.setTransactionIsolation("TRANSACTION_READ_COMMITTED");
    dataSource.setConnectionTestQuery("SELECT 1");
    dataSource.setIdleTimeout(0);
    return dataSource;
  }
}
