* [Temario, curso y recursos](https://drive.google.com/open?id=0BwkYx3waNF8VTGx3TzVwU3pkSUE)



Cómo levantar con docker-compose
================================

Requisitos: docker y docker-compose recientes (ej 17.10).

Con `docker-compose up` se levanta la BD vacía y el pgadmin4 en el puerto 80 local. Cuenta por defecto para entrar: curso.sql@buntplanet.com / cursosql.

La cuenta para el postgre es cursosql/cursosql, y la BD es modernsql.

Para importar el dump, si se tiene psql 9.6 en local sólo es hacer:

`gunzip -c star.sql.gz | psql --host localhost --user cursosql modernsql`

Sino se puede hacer desde el propio contenedor de postgre con:

`docker cp star.sql.gz modern-sql_db_1:/tmp`
`docker exec -it modern-sql_db_1 bash`
Y a continuacion la orden anterior. 
