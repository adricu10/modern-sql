-- MOLADURISMO I

SELECT *
FROM
      generate_series(
          '2007-02-01' :: TIMESTAMP,
          '2008-04-01' :: TIMESTAMP,
          '1 day' :: INTERVAL
      ) fechas,
  (VALUES
    ('a1', 'b1'),
    ('a2', 'b2'),
    ('a3', 'b3')
  ) AS cosas;


SELECT * FROM generate_series(
                   '2007-02-01' :: TIMESTAMP,
                   '2008-04-01' :: TIMESTAMP,
                   '1 day' :: INTERVAL
               ) fechas;
-- Values a pelo
--     http://modern-sql.com/use-case/query-drafting-without-table
--     http://modern-sql.com/use-case/select-without-from


-- COMMON TABLE EXPRESSIONS
-- with: ¿para qué?
-- http://www.postgresql.org/docs/current/static/queries-with.html
-- with como semantica en subselects, limpieza, ver la cadena de transformaciones
-- with recursivo (ideal para estructuras recursivas, o para generar datos que no se puedan generar con generate_series)
-- with sobre modificacion de bd con RETURNING

--The general form of a recursive WITH query is always a non-recursive term, then UNION (or UNION ALL), then a recursive term, where only the recursive term can contain a reference to the query's own output.
-- c is the output

WITH RECURSIVE factorial(n, factorial) AS (
  VALUES (1, 1)
  UNION ALL
  SELECT
    n + 1,
    (n + 1) * factorial
  FROM factorial
)
SELECT
  n,
  factorial
FROM factorial
LIMIT 5;









CREATE TABLE parts (
  part     TEXT,
  sub_part TEXT
);

INSERT INTO parts VALUES
  ('ordenador', 'pantalla'),
  ('ordenador', 'teclado'),
  ('ordenador', 'raton'),
  ('ordenador', 'cpu'),
  ('teclado', 'tecla'),
  ('teclado', 'cable'),
  ('teclado', 'carcasa'),
  ('raton', 'botones'),
  ('raton', 'cable'),
  ('raton', 'rueda'),
  ('cpu', 'placa base'),
  ('cpu', 'procesador'),
  ('cpu', 'carcasa'),
  ('cpu', 'carcasa'),
  ('pantalla', NULL),
  ('tecla', NULL),
  ('cable', NULL),
  ('carcasa', NULL),
  ('botones', NULL),
  ('rueda', NULL),
  ('placa base', NULL),
  ('procesador', NULL);

WITH RECURSIVE included_parts(part, sub_part) AS (
  SELECT
    part,
    sub_part
  FROM parts
  WHERE part = 'teclado'
  UNION ALL
  SELECT
    p.part,
    p.sub_part
  FROM included_parts pr, parts p
  WHERE p.part = pr.sub_part
)
SELECT
  sub_part,
  count(sub_part)
FROM included_parts
GROUP BY sub_part;


WITH RECURSIVE included_parts(sub_part, part) AS (
  SELECT
    sub_part,
    part
  FROM parts
  WHERE part = 'our_product'
  UNION ALL
  SELECT
    p.sub_part,
    p.part
  FROM included_parts pr, parts p
  WHERE p.part = pr.sub_part
)
DELETE FROM parts
WHERE part IN (SELECT part
               FROM included_parts);

-- UPSERT:

-- Predicate within UPDATE auxiliary statement
-- (row is still locked when the UPDATE predicate
-- isn't satisfied):
INSERT INTO upsert (key, val) VALUES (1, 'insert')
ON CONFLICT (key) UPDATE -- inference expression "(key)" mandatory for UPDATE variant
-- EXCLUDED.* carries forward effects of
-- INSERT BEFORE row-level triggers:
SET val = EXCLUDED .val
WHERE TARGET.val != 'delete';




WITH upsert AS (
  UPDATE spider_count
  SET tally = tally + 1
  WHERE date = 'today' AND spider = 'Googlebot'
  RETURNING *
)
INSERT INTO spider_count (spider, tally)
  SELECT
    'Googlebot',
    1
  WHERE NOT EXISTS(
      SELECT *
      FROM upsert
  );




-- # JUGAR CON LAS DIMENSIONES
-- Viajes nocturnos:

SELECT *
FROM star.fct_trip t
  INNER JOIN star.dim_hour h ON h.hour_id = t.start_hour_id
WHERE h.is_night_period;

-- Viajes en martes:

SELECT *
FROM star.fct_trip t
  INNER JOIN star.dim_day d ON d.day_id = t.start_day_id
WHERE d.day_in_week = 2;

SELECT
  h.is_night_period,
  sum(duration_minutes)
FROM star.fct_trip t
  INNER JOIN star.dim_day d ON d.day_id = t.start_day_id
  INNER JOIN star.dim_hour h ON h.hour_id = t.start_hour_id
WHERE d.day_in_week = 2
GROUP BY h.is_night_period;

SELECT
  h.is_night_period,
  sum(duration_minutes)
FROM star.fct_trip t
  INNER JOIN star.dim_hour h ON h.hour_id = t.start_hour_id
WHERE t.start_day_id BETWEEN 20150101 AND 20150131
GROUP BY h.is_night_period;

-- Viajes el último día del mes:

SELECT *
FROM star.fct_trip t
  INNER JOIN star.dim_day d ON d.day_id = t.start_day_id
WHERE d.last_day_of_month = d.day_date;

-- Clustering de terminals y viajes totales:

SELECT
  substr(dt.geohash, 0, 16),
  json_agg(DISTINCT dt.name) AS terminals,
  count(*)                   AS total_trips
FROM fct_trip ft
  INNER JOIN dim_terminal dt ON dt.id = ft.start_terminal_id
WHERE ft.start_day_id >= 20150801
GROUP BY substr(dt.geohash, 0, 16)
ORDER BY total_trips DESC;






-- Pivots y agregaciones:

SELECT
  dh.is_night_period                  AS nocturnal,
  sum(ds.price)
    FILTER (WHERE dd.day_in_week = 1) AS monday,
  sum(ds.price)
    FILTER (WHERE dd.day_in_week = 2) AS tuesday,
  sum(ds.price)
    FILTER (WHERE dd.day_in_week = 3) AS wednesday,
  sum(ds.price)
    FILTER (WHERE dd.day_in_week = 4) AS thursday,
  sum(ds.price)
    FILTER (WHERE dd.day_in_week = 5) AS friday,
  sum(ds.price)
    FILTER (WHERE dd.day_in_week = 6) AS saturday,
  sum(ds.price)
    FILTER (WHERE dd.day_in_week = 7) AS sunday,
  sum(ds.price)
    FILTER (WHERE dd.is_weekend)      AS in_week,
  sum(ds.price)
    FILTER (WHERE NOT dd.is_weekend)  AS weekends
FROM fct_trip ft
  INNER JOIN dim_subscription ds ON ds.id = ft.subscription_id
  INNER JOIN dim_day dd ON dd.day_id = ft.start_day_id
  INNER JOIN dim_hour dh ON dh.hour_id = ft.start_hour_id
GROUP BY dh.is_night_period;

--  - Afecta el tipo de bicicleta a la duración del viaje








-- GROUPING SETS:

SELECT
  dd.year_id,
  dd.month_id,
  dd.day_in_month,
  sum(ft.duration_minutes)
FROM star.fct_trip ft
  INNER JOIN star.dim_day dd ON dd.day_id = ft.start_day_id
GROUP BY GROUPING SETS (
  (dd.year_id, dd.month_id, dd.day_in_month),
  (dd.year_id, dd.month_id),
  (dd.year_id),
  ()
  );






SELECT
  dd.year_id,
  dd.month_id,
  dd.day_in_month,
  sum(ft.duration_minutes)
FROM star.fct_trip ft
  INNER JOIN star.dim_day dd ON dd.day_id = ft.start_day_id
GROUP BY dd.year_id, dd.month_id, dd.day_in_month
  UNION ALL

SELECT
  dd.year_id,
  dd.month_id,
  NULL,
  sum(ft.duration_minutes)
FROM star.fct_trip ft
  INNER JOIN star.dim_day dd ON dd.day_id = ft.start_day_id
GROUP BY dd.year_id, dd.month_id;








-- ROLLUP por fechas:

SELECT
  dd.year_id,
  dd.month_id,
  dd.day_in_month,
  sum(ft.duration_minutes)
FROM fct_trip ft
  INNER JOIN dim_day dd ON dd.day_id = ft.start_day_id
GROUP BY ROLLUP (dd.year_id, dd.month_id, dd.day_in_month);












-- CUBE de dimensiones no jerárquicas:

SELECT
  dt.name AS               terminal,
  ds.name AS               subscription,
  db.type AS               bike,
  sum(ft.duration_minutes) total_minutes
FROM fct_trip ft
  INNER JOIN dim_terminal dt ON dt.id = ft.start_terminal_id
  INNER JOIN dim_subscription ds ON ds.id = ft.subscription_id
  INNER JOIN dim_bike db ON db.id = ft.bike_id
WHERE ft.start_day_id >= 20151001
GROUP BY CUBE (dt.name, ds.name, db.type)
HAVING sum(ft.duration_minutes) > 0;

-- equivalente a

SELECT
  dt.name AS               terminal,
  ds.name AS               subscription,
  db.type AS               bike,
  sum(ft.duration_minutes) total_minutes
FROM fct_trip ft
  INNER JOIN dim_terminal dt ON dt.id = ft.start_terminal_id
  INNER JOIN dim_subscription ds ON ds.id = ft.subscription_id
  INNER JOIN dim_bike db ON db.id = ft.bike_id
WHERE ft.start_day_id >= 20151001
GROUP BY GROUPING SETS (
  (dt.name, ds.name, db.type),
  (dt.name, ds.name),
  (ds.name, db.type),
  (dt.name, db.type),
  (dt.name),
  (ds.name),
  (db.type),
  ()
  )
HAVING sum(ft.duration_minutes) > 0;





-- CREATE TABLE CON ESTEROIDES

CREATE TEMP TABLE tmp_fct_trip (
  LIKE star.fct_trip
) ON COMMIT DROP;







DROP TABLE IF EXISTS star.fct_durations_per_day;
CREATE TABLE star.fct_durations_per_day AS
  SELECT
    year_month_id,
    start_day_id,
    sum(duration_minutes) AS duration
  FROM star.fct_trip t
    INNER JOIN star.dim_day dd ON dd.day_id = t.start_day_id
  GROUP BY start_day_id, year_month_id;
















-- WINDOW FUNCTIONS
-- Abrir en el navegador
--    http://www.postgresql.org/docs/9.5/static/sql-expressions.html#SYNTAX-WINDOW-FUNCTIONS
--    http://www.postgresql.org/docs/9.5/static/functions-window.html
--    http://www.postgresql.org/docs/9.5/static/ SQL - SELECT.html
--     https://sqlschool.modeanalytics.com/advanced/window-functions/

SELECT
  year_month_id,
  start_day_id,
  duration,
  sum(duration)
  OVER (
    PARTITION BY year_month_id
    ORDER BY start_day_id
    ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
--      ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING
--      ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
--     ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
  ) AS duration_sum
FROM star.fct_durations_per_day
ORDER BY start_day_id;




WITH ranking AS (
    SELECT
      year_month_id,
      start_day_id,
      duration,
      row_number()
      OVER (
        PARTITION BY year_month_id
        ORDER BY duration DESC
        --     ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING
        --      ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
        ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
      ) AS rn
    FROM star.fct_durations_per_day
) SELECT * FROM ranking WHERE rn <= 5
ORDER BY year_month_id, rn;
-- ORDER BY year_month_id, duration DESC;







-- query base a la que ir añadiendo las cosas que tienen comentarios
SELECT
  *,
  row_number()
  OVER ()                                                total_order,
  -- ilustrar que con esto basta para hacer window function
  row_number()
  OVER (
    ORDER BY start_day_id)                            AS total_order_2,
  -- mejor con ordenacion explícita
  row_number()
  OVER (
    ORDER BY start_day_id
    ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS total_order_3,
  -- todo explícito
  row_number()
  OVER (PARTITION BY year_month_id
    ORDER BY start_day_id
    ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS order_in_month -- una window function completa
FROM star.fct_durations_per_day
ORDER BY start_day_id
;

-- dos ramificaciones
-- ver en lugar de row_number rank, dense_rank, percent_rank
-- ver un running total por particiones, para ver la diferencia de ROW BETWEEN XXXXXXXX

-- de aquí sacar un TOP TEN es fácil

-- row_number()
-- rank()
-- dense_rank()
-- percent_rank()
-- cume_dist()
-- ntile(num_bkcets, integer)
-- lag(value, offset, default)
-- lead
-- first_value(value)
-- last_value(value)
-- nth_value(value, nth)


-- OTRAS QUERIES A PEDIR:

-- - Running totals
-- - sum/avg/count por partición
-- - lag/lead:la media de la diferencia con el anterior nos dice el RITMO DE salida

--   Esto puede ser interesante para detectar estaciones cuyo ritmo de salida medio supera al de llegada, por lo que corren el riesgo de quedarse sin bicis.

WITH trip_start_pairs(terminal, start_time, next_start_time) AS (
    SELECT
      dt.name,
      ft.start_time,
      lead(ft.start_time)
      OVER (
        ORDER BY dt.name, ft.start_day_id, ft.start_time
      )
    FROM star.fct_trip ft
      INNER JOIN star.dim_terminal dt ON dt.id = ft.start_terminal_id
    WHERE ft.start_day_id >= 20151001
), trip_start_deltas AS (
    SELECT
      terminal,
      next_start_time - start_time AS delta_duration
    FROM trip_start_pairs
)
SELECT
  terminal,
  avg(delta_duration)
FROM trip_start_deltas
GROUP BY terminal
ORDER BY avg(delta_duration);

-- Agregación EVERY:

SELECT
  DISTINCT
  dt.name,
  every(db.type = 'city')
  OVER (PARTITION BY dt.name),
  bool_and(db.type = 'city')
  OVER (PARTITION BY dt.name),
  bool_or(db.type = 'road')
  OVER (PARTITION BY dt.name)
FROM fct_trip ft
  INNER JOIN dim_bike db ON db.id = ft.bike_id
  INNER JOIN dim_terminal dt ON dt.id = ft.start_terminal_id
WHERE ft.start_day_id >= 20151001
ORDER BY dt.name;

-- NUEVAS QUERIES

WITH aggregated(year_month_id, start_day_id, duration_minutes) AS (
    SELECT
      year_month_id,
      start_day_id,
      sum(duration_minutes)
    FROM fct_trip t
      INNER JOIN dim_day dd ON dd.day_id = t.start_day_id
    GROUP BY start_day_id, year_month_id
)
SELECT
  *,
  sum(duration_minutes)
  OVER (
    PARTITION BY year_month_id
    ORDER BY start_day_id
    ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
  )
FROM aggregated
ORDER BY start_day_id;